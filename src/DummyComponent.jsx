import React from 'react';
/*
export const Tax = ({value, country}) => {
    const tax = country === 'COL' ? 1.19 : 1.1;
    return <div>{value * tax}</div>
}*/

const TaxComponent = ({value}) => {
    return <div>{value}</div>
}

export const TaxContainer = ({value, country}) => {
    const tax = country === 'COL' ? 1.19 : 1.1;
    const calculated = value * tax;
    return <TaxComponent value={calculated}/>
} 