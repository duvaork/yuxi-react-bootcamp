import logo from './logo.svg';
import './App.css';
//import ClassComponent from './ClassComponent.jsx';
import FunctionComponent from './FunctionComponent.jsx';
import HigherOrderComponent from './HigherOrderComponent';
import { useState } from 'react';

const WrappedClassComponent = HigherOrderComponent(FunctionComponent);

function App() {
  const [showClassComponent, setShowClassComponent] = useState(true);
  const [loading, setLoading] = useState(true);
setTimeout( () => setLoading(false), 2000);

  return (
    <div className="App">
      {loading && <h1>Loading</h1>}
      {showClassComponent &&<WrappedClassComponent 
      name = {'Class component'} 
      onFinish = {() => setShowClassComponent(false)}
      show = {showClassComponent}
      />}
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
