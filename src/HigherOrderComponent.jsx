import React from "react";

export default function HigherOrderComponent(WrappedComponent){
    return class extends React.Component {
        constructor(props) {
          super(props);
          this.state = {};
        }
    
        render() {
          // ... y renderiza el componente envuelto con los datos frescos!
          // Toma nota de que pasamos cualquier prop adicional
          return <div style={{backgroundColor: '#03DAC6'}}><WrappedComponent data={this.state.data} {...this.props}/></div>;
        }
      };
}