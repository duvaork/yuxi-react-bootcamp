import React, { useEffect, useState } from "react";

const FunctionComponent = ({show, onFinish}) => {
    console.log("Render");
    const [counter, setCounter] = useState(0);
    
    useEffect(() => {        
        console.log("UseEffect as mount");
        const interval = setInterval(
            ()=> {
                setCounter(counter => counter + 1);
            },
            1000
        );
        return () => {
            console.log("unmount");
            clearInterval(interval);
        }
    }, []);

    useEffect(() => {
        console.log("UseEffect as did update");
        if (counter > 3){
            onFinish();
        }
        return () => {
            console.log("Clean before use Effect");
        }
    }, [counter, onFinish]);

    useEffect(() => {
        console.log('Show changed');
    }, [show]);

    return <h1 onClick={() => setCounter(counter+1)}>Hi! My name is {counter}</h1>;
}

export default FunctionComponent;