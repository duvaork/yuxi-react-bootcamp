import React from "react";

export default class ClassComponent extends React.Component{

    constructor(props){
        super(props);
        console.log(props);
        this.state = { counter: 0};
    }

    render(){
        console.log('render');
        return <h1>Hi! My name is {this.props.name} {this.state.counter}</h1>;
    }

    componentDidMount(){
        console.log("Mounted");
        this.startCounter();
    }

    startCounter(){
        const interval = setInterval(
            ()=> {
                this.setState({counter: this.state.counter + 1});
            },
            1000
        );
        this.setState({...this.state,interval});
    }

    componentDidUpdate(prevProps, prevState) {
        //console.log("changed");
        if (this.props.counter !== prevState.counter && prevState.counter > 3) {
            this.props.onFinish();
        }
    }

    componentWillUnmount(){
        clearInterval(this.state.interval);
        console.log("Bye");
    }

}
/*
const functionComponent = (props) => {
    return <h1>Hi! My name is {props.name}</h1>;
}

export default functionComponent;*/